<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

$domainCheck = $_SERVER['SERVER_NAME'];

if ( $domainCheck == 'localhost')
{
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'prologiq_bpower' );

	/** MySQL database username */
	define( 'DB_USER', 'root' );

	/** MySQL database password */
	define( 'DB_PASSWORD', 'root' );

	/** MySQL hostname */
	define( 'DB_HOST', 'localhost' );

	/** Database Charset to use in creating database tables. */
	define( 'DB_CHARSET', 'utf8mb4' );

	/** The Database Collate type. Don't change this if in doubt. */
	define( 'DB_COLLATE', '' );

}else {
	// ** MySQL settings - You can get this info from your web host ** //
	/** The name of the database for WordPress */
	define( 'DB_NAME', 'prologiq_bpower' );

	/** MySQL database username */
	define( 'DB_USER', 'prologiq_dev1Master' );

	/** MySQL database password */
	define( 'DB_PASSWORD', '@PrologiQ2906' );

	/** MySQL hostname */
	define( 'DB_HOST', 'localhost' );

	/** Database Charset to use in creating database tables. */
	define( 'DB_CHARSET', 'utf8mb4' );

	/** The Database Collate type. Don't change this if in doubt. */
	define( 'DB_COLLATE', '' );

}


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '6L+Rw(>prqMa4VFc3R=}0^2Ra1d-f2V{aAnx`?qsF>E5RuQM+}]H6gA;>PP!e|VU' );
define( 'SECURE_AUTH_KEY',  ' BaJN0%*jwIOqX6DrYY.SM5-/8:Pk-[J;9^bY#,|W%VYHE<j(iLR:M:R&wtcIY]n' );
define( 'LOGGED_IN_KEY',    '*@mK f; /?tnMCJ{}cDWp}6!pagDxdMxhG/z%$x-b*yd!w(ZR%Ki@HDd| *r7~Y@' );
define( 'NONCE_KEY',        'U2VyTI8>UtbT@/<h$MJY.W.p4dv4^:<XX/H%&%OqHuty_.Ms2p-L,Lc={p^_5vZ2' );
define( 'AUTH_SALT',        ']dZ{D&4#}R#XFdZJ;P`Cm;$L<}wVLfs|[MtvnbUst$B{5#!$|}{zm}IeM%/hWf-&' );
define( 'SECURE_AUTH_SALT', '=]?V VEl*()lx~;&bRv(FP./Vr$cjUjYV~jpAY3F)L2n(fu#,R4aNbe^HcvQQlU;' );
define( 'LOGGED_IN_SALT',   '~^nlnAJ>8TF[KU(|;I}:#p>L?k 1:!SQ2u[^M~)t90UA0Vjl)El&p%ZhcY235<R`' );
define( 'NONCE_SALT',       '=^1$)I>*9,fxWkVsi;G>noAZ7JU2xt3 4g2H|p3/pRgdAM)CG[FafU2P7+/M`?=~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bpw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
